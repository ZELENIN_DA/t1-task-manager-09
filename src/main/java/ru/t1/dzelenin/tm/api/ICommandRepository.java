package ru.t1.dzelenin.tm.api;

import ru.t1.dzelenin.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
